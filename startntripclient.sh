#!/bin/bash
#
# $Id$
# Purpose: Start ntripclient

Server='www.euref-ip.net'
Port=80
Stream='KRA100POL0'
User='llewellyn'
Password='Fernandes@789'
SerialDevice='/dev/ttyUSB0'
BaudRate=115200

DateStart=`date -u '+%s'`
SleepMin=10     # Wait min sec for next reconnect try
SleepMax=10000  # Wait max sec for next reconnect try

(while true; do
  echo "Starting Ntrip Client"
  ./ntripclient -s $Server -r $Port -m $Stream -u $User -p $Password -D $SerialDevice -B $BaudRate
  if test $? -eq 0; then DateStart=`date -u '+%s'`; fi
  DateCurrent=`date -u '+%s'`
  SleepTime=`echo $DateStart $DateCurrent | awk '{printf("%d",($2-$1)*0.02)}'`
  if test $SleepTime -lt $SleepMin; then SleepTime=$SleepMin; fi
  if test $SleepTime -gt $SleepMax; then SleepTime=$SleepMax; fi
  # Sleep 2 percent of outage time before next reconnect try
  sleep $SleepTime
done)

